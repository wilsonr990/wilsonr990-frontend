import React from 'react';
import {mount} from 'enzyme';
import '../../../__tests/setupTests';
import 'jest-styled-components';
import MonthlyProfitSummaryPage from '../MonthlyProfitSummaryPage';
import {mockedServer, rest, waitExpectWithAct} from '../../../__tests/setupTests';
import {Endpoints} from '../../../endpoints';
import LockingError from '../../../Common/Components/LockingError';
import {defaultAccount, profitSummaryRow} from '../../__tests/modelData';

describe('<MonthlyProfitSummaryPage />', () => {
  it('Should be displayed correctly', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    const year = (new Date()).getFullYear();
    const month = (new Date()).getMonth();
    mockedServer.use(rest.get(Endpoints.monthlyProfitSummary(year, month + 1, defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));

    const wrapper = mount(<MonthlyProfitSummaryPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(2);
    });

    expect(wrapper.find('h1').text()).toBe('PROFIT SUMMARY');
    const buttons = wrapper.find('button');
    expect(buttons).toHaveLength(2);
    expect(buttons.at(0).text()).toBe('Go to Transactions');
    const inputs = wrapper.find('input');
    expect(inputs).toHaveLength(2);
    expect(inputs.at(0).prop('value')).toBe(year);
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    expect(inputs.at(1).prop('value')).toBe(months[month]);
  });

  it('Should fail and not show table when summary data fails', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));

    const year = (new Date()).getFullYear();
    const month = (new Date()).getMonth();
    mockedServer.use(rest.get(Endpoints.monthlyProfitSummary(year, month + 1, defaultAccount.id), (req, res) => {
      return res.networkError('Error Message');
    }));

    const wrapper = mount(<MonthlyProfitSummaryPage/>);

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(2);
    });

    await waitExpectWithAct(() => {
      expect(wrapper.update().find(LockingError).text()).toContain('Network Error');
      expect(wrapper.update().find(LockingError).find('button').text()).toContain('Refresh');
    });
  });

  it('Should display table when monthly profit data is received', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    const year = (new Date()).getFullYear();
    const month = (new Date()).getMonth();
    mockedServer.use(rest.get(Endpoints.monthlyProfitSummary(year, month + 1, defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([profitSummaryRow]));
    }));

    const wrapper = mount(<MonthlyProfitSummaryPage/>);

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(2);
    });

    await waitExpectWithAct(() => {
      const trs = wrapper.update().find('th');
      expect(trs).toHaveLength(6);
      expect(trs.at(0).text().trim()).toBe('Stock Code');
      expect(trs.at(1).text().trim()).toBe('Avg Bought Price');
      expect(trs.at(2).text().trim()).toBe('Avg Sold Price');
      expect(trs.at(3).text().trim()).toBe('No Shares');
      expect(trs.at(4).text().trim()).toBe('Profit');
      expect(trs.at(5).text().trim()).toBe('Profit Percentage');
    });

    await waitExpectWithAct(() => {
      const tds = wrapper.update().find('td');
      expect(tds).toHaveLength(9);
      expect(tds.at(0).text().trim()).toBe('ABC');
      expect(tds.at(1).text().trim()).toBe('10.00 AED');
      expect(tds.at(2).text().trim()).toBe('9.00 AED');
      expect(tds.at(3).text().trim()).toBe('1');
      expect(tds.at(4).text().trim()).toBe('1.00 AED');
      expect(tds.at(5).text().trim()).toBe('10.00 %');
      expect(tds.at(6).text().trim()).toBe('Total Equity');
      expect(tds.at(7).text().trim()).toBe('1.00');
      expect(tds.at(8).text().trim()).toBe('');
    });
  });

  it('Should redirect to Transactions when Go to Transactions is clicked', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    const year = (new Date()).getFullYear();
    const month = (new Date()).getMonth();
    mockedServer.use(rest.get(Endpoints.monthlyProfitSummary(year, month + 1, defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([profitSummaryRow]));
    }));

    const wrapper = mount(<MonthlyProfitSummaryPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(2);
    });

    const HistoryButton = wrapper.find('button').at(0);
    HistoryButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(window.location.assign).toBeCalledWith(Endpoints.stockTransactionsPage());
    });
  });

  it('Should refresh table when filter changes', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    const year = (new Date()).getFullYear();
    const month = (new Date()).getMonth();
    mockedServer.use(rest.get(Endpoints.monthlyProfitSummary(year, month + 1, defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([profitSummaryRow]));
    }));
    mockedServer.use(rest.get(Endpoints.monthlyProfitSummary(year - 1, month + 1), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([profitSummaryRow, {...profitSummaryRow, stockCode: 'DEF'}]));
    }));
    mockedServer.use(rest.get(Endpoints.monthlyProfitSummary(year - 1, month + 2), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([profitSummaryRow, {...profitSummaryRow, stockCode: 'GHI'}]));
    }));

    const wrapper = mount(<MonthlyProfitSummaryPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(2);
    });

    const inputs = wrapper.find('input');
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    inputs.at(0).simulate('change', {target: {value: year - 1}});

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(3);
    });

    inputs.at(1).simulate('change', {target: {value: months[month + 1]}});

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(4);
    });

    await waitExpectWithAct(() => {
      const trs = wrapper.update().find('th');
      expect(trs).toHaveLength(6);
      expect(trs.at(0).text().trim()).toBe('Stock Code');
      expect(trs.at(1).text().trim()).toBe('Avg Bought Price');
      expect(trs.at(2).text().trim()).toBe('Avg Sold Price');
      expect(trs.at(3).text().trim()).toBe('No Shares');
      expect(trs.at(4).text().trim()).toBe('Profit');
      expect(trs.at(5).text().trim()).toBe('Profit Percentage');
    });

    await waitExpectWithAct(() => {
      const tds = wrapper.update().find('td');
      expect(tds).toHaveLength(15);
      expect(tds.at(0).text().trim()).toBe('ABC');
      expect(tds.at(1).text().trim()).toBe('10.00 AED');
      expect(tds.at(2).text().trim()).toBe('9.00 AED');
      expect(tds.at(3).text().trim()).toBe('1');
      expect(tds.at(4).text().trim()).toBe('1.00 AED');
      expect(tds.at(5).text().trim()).toBe('10.00 %');
      expect(tds.at(6).text().trim()).toBe('GHI');
      expect(tds.at(7).text().trim()).toBe('10.00 AED');
      expect(tds.at(8).text().trim()).toBe('9.00 AED');
      expect(tds.at(9).text().trim()).toBe('1');
      expect(tds.at(10).text().trim()).toBe('1.00 AED');
      expect(tds.at(11).text().trim()).toBe('10.00 %');
      expect(tds.at(12).text().trim()).toBe('Total Equity');
      expect(tds.at(13).text().trim()).toBe('2.00');
      expect(tds.at(14).text().trim()).toBe('');
    });
  });
});
