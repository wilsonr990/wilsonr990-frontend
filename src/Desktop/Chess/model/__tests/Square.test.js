import React from 'react';
import Square from '../Square';
import {shallow} from 'enzyme';
import {expectToThrow} from '../../../../__tests/setupTests';
import '../../../../__tests/setupTests';
import 'jest-styled-components';
import {act} from '@testing-library/react';

describe('<Square />', () => {
  it('Should be black if Black is true', async () => {
    const wrapper = shallow(<Square black={true} location={'a8'}>
      <div/>
    </Square>);

    await act(async () => wrapper.prop('onDragOver')({preventDefault: jest.fn()}));

    expect(wrapper).toHaveStyleRule('background', 'gray');
  });

  it('Should be white if Black is false', async () => {
    const wrapper = shallow(<Square black={false} location={'a8'}>
      <div/>
    </Square>);

    await act(async () => wrapper.prop('onDragOver')({preventDefault: jest.fn()}));

    expect(wrapper).toHaveStyleRule('background', 'white');
  });

  it('Should fail if exists children', () => {
    expectToThrow(() => shallow(<Square>
      <div/>
      <div/>
    </Square>),
    'Warning: Failed prop type: `Square` should contain one children (2 received)');
  });
});
