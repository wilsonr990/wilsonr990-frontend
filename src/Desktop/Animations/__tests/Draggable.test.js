import React from 'react';
import Draggable from '../Draggable';
import {mount, shallow} from 'enzyme';
import {Floating} from '../../../Common/Components/Floating';
import {expectToThrow} from '../../../__tests/setupTests';

describe('<Draggable />', () => {
  it('Fail when more than one children are provided', () => {
    expectToThrow(() => shallow(
      <Draggable>
        <div>1</div>
        <div>2</div>
      </Draggable>,
    ), 'Warning: Failed prop type: `Draggable` should contain one children (2 received)');
  });

  it('Fail when no children are provided', () => {
    expectToThrow(() => shallow(<Draggable/>),
      'Warning: Failed prop type: `Draggable` should contain one children (0 received)',
    );
  });

  it('Renders correctly', () => {
    const wrapper = mount(
      <Draggable id={'test'}>
        <div>item</div>
      </Draggable>,
    );

    const floating = wrapper.find(Floating);
    expect(floating.exists()).toBeTruthy();
    expect(wrapper.render().find('div').html()).toBe('<div id="test"><div>item</div></div>');
  });

  it('Pass x, y and z properties to Floating', () => {
    const callback = jest.fn();

    const wrapper = mount(
      <Draggable onDragged={callback} x={50} y={50} z={1} id={'test'}>
        <div>item</div>
      </Draggable>,
    );

    const floating = wrapper.find(Floating);
    expect(floating.exists()).toBeTruthy();
    expect(floating.props().x).toBe(50);
    expect(floating.props().y).toBe(50);
    expect(floating.props().z).toBe(1);
    expect(wrapper.render().find('div').html()).toBe('<div id="test"><div>item</div></div>');
    expect(callback).toBeCalledTimes(0);
  });

  // Todo: simulating drag is not supported. Haven't found a proper way to test it
});
