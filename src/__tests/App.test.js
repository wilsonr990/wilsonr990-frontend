import React from 'react';
import App from '../App';
import {mount} from 'enzyme';
import '../__tests/setupTests';
import DeskPage from '../Desktop/Desk/DeskPage';
import {MemoryRouter} from 'react-router-dom';
import PaperFormPage from '../Desktop/PaperForm/PaperFormPage';
import LanguageCardsPage from '../Desktop/LanguageCards/LanguageCardsPage';
import StockJournalPage from '../Stocks/Journal/StockJournalPage';
import MonthlyProfitSummaryPage from '../Stocks/Profit Summary/MonthlyProfitSummaryPage';
import {mockedServer, rest, waitExpectWithAct} from './setupTests';
import {Endpoints} from '../endpoints';
import {accountSummary, defaultAccount} from '../Stocks/__tests/modelData';
import StockTransactionsPage from '../Stocks/Transactions/StockTransactionsPage';
import {Action} from 'rxjs/internal/scheduler/Action';
import LoginPage from '../Login/LoginPage';
import {act} from '@testing-library/react';
import ReLoginError from '../Common/Components/ReLoginError';

describe('<App/>', () => {
  it('renders DeskPage by default', async () => {
    window.sessionStorage.getItem.mockReturnValueOnce(null);
    window.location.pathname = '/';

    const wrapper = mount(<MemoryRouter initialEntries={['/']}>
      <App/>
    </MemoryRouter>);

    const deskPage = wrapper.find(DeskPage);
    expect(deskPage.exists()).toBeTruthy();

    expect(window.sessionStorage.getItem).toBeCalledTimes(1);
  });

  it('renders DeskPage in desktop path', async () => {
    window.sessionStorage.getItem.mockReturnValueOnce(null);
    window.location.pathname = '/desktop';

    const wrapper = mount(<MemoryRouter initialEntries={['/desktop']}>
      <App/>
    </MemoryRouter>);

    const deskPage = wrapper.find(DeskPage);
    expect(deskPage.exists()).toBeTruthy();

    expect(window.sessionStorage.getItem).toBeCalledTimes(1);
  });

  it('renders LanguageCardsPage in LanguageCards/:url path', () => {
    window.sessionStorage.getItem.mockReturnValueOnce(null);
    window.location.pathname = '/languageCards/English';

    const wrapper = mount(<MemoryRouter initialEntries={['/LanguageCards/English']}>
      <App/>
    </MemoryRouter>);

    const languageCardsPage = wrapper.find(LanguageCardsPage);
    expect(languageCardsPage.exists()).toBeTruthy();

    expect(window.sessionStorage.getItem).toBeCalledTimes(1);
  });

  it('redirects to login in pages that require login', () => {
    window.sessionStorage.getItem.mockReturnValueOnce(null);
    window.location.pathname = '/anyPathNotListedInOpenPaths';

    const wrapper = mount(<MemoryRouter initialEntries={['/anyPathNotListedInOpenPaths']}>
      <App/>
    </MemoryRouter>);

    const paperFormPage = wrapper.find(PaperFormPage);
    expect(paperFormPage.exists()).toBeFalsy();

    expect(window.sessionStorage.getItem).toBeCalledTimes(1);
    expect(window.sessionStorage.setItem).toBeCalledTimes(1);
    expect(window.sessionStorage.setItem).toBeCalledWith('destination', '/anyPathNotListedInOpenPaths');
    expect(window.location.assign).toBeCalledTimes(1);
    expect(window.location.assign).toBeCalledWith('/login');
  });

  it('renders login in /login path', () => {
    window.sessionStorage.getItem.mockReturnValueOnce(null);
    window.location.pathname = '/login';

    const wrapper = mount(<MemoryRouter initialEntries={['/login']}>
      <App/>
    </MemoryRouter>);

    const loginPage = wrapper.find(LoginPage);
    expect(loginPage.exists()).toBeTruthy();

    expect(window.sessionStorage.getItem).toBeCalledTimes(1);
  });

  it('redirects to destination path once logged in', async () => {
    window.sessionStorage.getItem.mockReturnValueOnce(null).mockReturnValueOnce('/destination');
    window.location.pathname = '/login';

    const wrapper = mount(<MemoryRouter initialEntries={['/login']}>
      <App/>
    </MemoryRouter>);

    const loginPage = wrapper.find(LoginPage);
    expect(loginPage.exists()).toBeTruthy();

    act(() => loginPage.props().setToken('{"token": "theBearertoken"}'));

    expect(window.sessionStorage.getItem).toBeCalledTimes(2);
    expect(window.sessionStorage.setItem).toBeCalledTimes(1);
    expect(window.sessionStorage.setItem).toBeCalledWith('token', '\"{\\\"token\\\": \\\"theBearertoken\\\"}\"');
    expect(window.location.assign).toBeCalledTimes(1);
    expect(window.location.assign).toBeCalledWith('/destination');
  });

  it('redirects to / if not destination saved once logged', async () => {
    window.sessionStorage.getItem.mockReturnValueOnce(null).mockReturnValueOnce(null);
    window.location.pathname = '/login';

    const wrapper = mount(<MemoryRouter initialEntries={['/login']}>
      <App/>
    </MemoryRouter>);

    const loginPage = wrapper.find(LoginPage);
    expect(loginPage.exists()).toBeTruthy();

    act(() => loginPage.props().setToken('theBearertoken'));

    expect(window.sessionStorage.getItem).toBeCalledTimes(2);
    expect(window.sessionStorage.setItem).toBeCalledTimes(1);
    expect(window.sessionStorage.setItem).toBeCalledWith('token', '\"theBearertoken\"');
    expect(window.location.assign).toBeCalledTimes(1);
    expect(window.location.assign).toBeCalledWith('/');
  });

  it('login redirects to destination if token is already valid', () => {
    window.sessionStorage.getItem.mockReturnValueOnce('{"token": "theBearertoken"}').mockReturnValueOnce('/destination');
    window.location.pathname = '/login';

    const wrapper = mount(<MemoryRouter initialEntries={['/login']}>
      <App/>
    </MemoryRouter>);

    const loginPage = wrapper.find(LoginPage);
    expect(loginPage.exists()).toBeTruthy();

    expect(window.sessionStorage.getItem).toBeCalledTimes(2);
    expect(window.location.assign).toBeCalledTimes(1);
    expect(window.location.assign).toBeCalledWith('/destination');
  });

  it('login redirects to / if token is already valid and no destination set', () => {
    window.sessionStorage.getItem.mockReturnValueOnce('{"token": "theBearertoken"}').mockReturnValueOnce(null);
    window.location.pathname = '/login';

    const wrapper = mount(<MemoryRouter initialEntries={['/login']}>
      <App/>
    </MemoryRouter>);

    const loginPage = wrapper.find(LoginPage);
    expect(loginPage.exists()).toBeTruthy();

    expect(window.sessionStorage.getItem).toBeCalledTimes(2);
    expect(window.location.assign).toBeCalledTimes(1);
    expect(window.location.assign).toBeCalledWith('/');
  });

  it('redirects to destination when logging out', async () => {
    window.sessionStorage.getItem.mockReturnValueOnce('{"token": "theBearertoken"}').mockReturnValueOnce('/destination');
    window.location.pathname = '/logout';

    const wrapper = mount(<MemoryRouter initialEntries={['/logout']}>
      <App/>
    </MemoryRouter>);

    const logoutMessage = wrapper.text();
    expect(logoutMessage).toEqual('Logged Out');

    expect(window.sessionStorage.getItem).toBeCalledTimes(1);
    expect(window.sessionStorage.clear).toBeCalledTimes(1);
    expect(window.history.back).toBeCalledTimes(1);
  });

  it('renders PaperFormPage in deskpage/:name path', () => {
    window.sessionStorage.getItem.mockReturnValueOnce('{"token": "theBearertoken"}');
    window.location.pathname = '/paperForm/something';

    const wrapper = mount(<MemoryRouter initialEntries={['/paperForm/something']}>
      <App/>
    </MemoryRouter>);

    const paperFormPage = wrapper.find(PaperFormPage);
    expect(paperFormPage.exists()).toBeTruthy();

    expect(window.sessionStorage.getItem).toBeCalledTimes(1);
  });

  it('renders StockTransactionsPage in Stocks path', async () => {
    window.sessionStorage.getItem.mockReturnValueOnce('{"token": "theBearertoken"}');
    window.location.pathname = '/stocks/transactions';

    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Action.DEPOSIT, Action.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<MemoryRouter initialEntries={['/stocks/transactions']}>
      <App/>
    </MemoryRouter>);

    const stockJournalPage = wrapper.find(StockTransactionsPage);
    expect(stockJournalPage.exists()).toBeTruthy();

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(4);
    });

    expect(window.sessionStorage.getItem).toBeCalledTimes(1);
  });

  it('renders StockJournalPage in Stocks path', async () => {
    window.sessionStorage.getItem.mockReturnValueOnce('{"token": "theBearertoken"}');
    window.location.pathname = '/stocks/journal';

    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stocksJournal(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));

    const wrapper = mount(<MemoryRouter initialEntries={['/stocks/journal']}>
      <App/>
    </MemoryRouter>);

    const stockJournalPage = wrapper.find(StockJournalPage);
    expect(stockJournalPage.exists()).toBeTruthy();

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(2);
    });

    expect(window.sessionStorage.getItem).toBeCalledTimes(1);
  });

  it('renders MonthlyProfitSummaryPage in /Profit/Summary path', async () => {
    window.sessionStorage.getItem.mockReturnValueOnce('{"token": "theBearertoken"}');
    window.location.pathname = '/stocks/profitSummary';

    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    const year = (new Date()).getFullYear();
    const month = (new Date()).getMonth();
    mockedServer.use(rest.get(Endpoints.monthlyProfitSummary(year, month + 1, defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));

    const wrapper = mount(<MemoryRouter initialEntries={['/stocks/profitSummary']}>
      <App/>
    </MemoryRouter>);

    const monthlySummaryPage = wrapper.find(MonthlyProfitSummaryPage);
    expect(monthlySummaryPage.exists()).toBeTruthy();

    await waitExpectWithAct(() => {
      expect(monthlySummaryPage.update().exists()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(2);
    });

    expect(window.sessionStorage.getItem).toBeCalledTimes(1);
  });

  it('Should show error if authentication is required in endpoint. Assume token expired', async () => {
    window.sessionStorage.getItem.mockReturnValueOnce('{"token": "theBearertoken"}');
    window.location.pathname = '/stocks/profitSummary';

    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(401), ctx.json({}));
    }));

    const wrapper = mount(<MemoryRouter initialEntries={['/stocks/profitSummary']}>
      <App/>
    </MemoryRouter>);

    const monthlySummaryPage = wrapper.find(MonthlyProfitSummaryPage);
    expect(monthlySummaryPage.exists()).toBeTruthy();

    await waitExpectWithAct(() => {
      expect(monthlySummaryPage.update().exists()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(1);
    });

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(wrapper.find(ReLoginError).text()).toContain('ExpiredAuthentication error. Please login againLogin Again');
      expect(wrapper.find(ReLoginError).find('button').text()).toContain('Login Again');
    });
  });

  it('Should navigate to logout after action is clicked in 401 error', async () => {
    window.sessionStorage.getItem.mockReturnValueOnce('{"token": "theBearertoken"}');
    window.location.pathname = '/stocks/profitSummary';

    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(401), ctx.json({}));
    }));

    const wrapper = mount(<MemoryRouter initialEntries={['/stocks/profitSummary']}>
      <App/>
    </MemoryRouter>);

    const monthlySummaryPage = wrapper.find(MonthlyProfitSummaryPage);
    expect(monthlySummaryPage.exists()).toBeTruthy();

    await waitExpectWithAct(() => {
      expect(monthlySummaryPage.update().exists()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(1);
    });

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(wrapper.find(ReLoginError).find('button').text()).toContain('Login Again');
    });

    const actionButton = wrapper.find(ReLoginError).find('button');
    actionButton.simulate('click');
    await waitExpectWithAct(() => {
      expect(window.location.assign).toBeCalledWith(`/logout`);
    });
  });
});
